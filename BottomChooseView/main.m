//
//  main.m
//  BottomChooseView
//
//  Created by frank吴 on 2018/12/5.
//  Copyright © 2018 BOC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
