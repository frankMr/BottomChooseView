//
//  GoodsChooseSheet.h
//  SeaTradeProficient
//
//  Created by Bocweb on 2017/10/16.
//  Copyright © 2017年 boc.company. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ChooseModel.h"

typedef void(^GetSelectName)(ChooseModel *chooseModel);

@interface ChooseSheet : UIView

@property(nonatomic,strong) NSMutableArray *dataArray;

@property (nonatomic, copy) GetSelectName nameBlock;

@end
