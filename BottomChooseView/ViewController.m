//
//  ViewController.m
//  BottomChooseView
//
//  Created by frank吴 on 2018/12/5.
//  Copyright © 2018 BOC. All rights reserved.
//

#import "ViewController.h"
#import "ChooseModel.h"
#import "ChooseSheet.h"

@interface ViewController ()
@property(nonatomic,strong) NSMutableArray   *dataArray;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    for (NSInteger i=0; i<5; i++) {
        ChooseModel *model = [[ChooseModel alloc] init];
        model.title = [NSString stringWithFormat:@"第%ld个",(long)i];
        [self.dataArray addObject:model];
    }
    
}
- (IBAction)showViewAction:(UIButton *)sender {
    ChooseSheet *chooseSheet = [[ChooseSheet alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    [chooseSheet setDataArray:self.dataArray];
    [self.view.window addSubview:chooseSheet];
}
- (NSMutableArray *)dataArray{
    if (!_dataArray) {
        _dataArray = [NSMutableArray new];
    }
    return _dataArray;
}

@end
