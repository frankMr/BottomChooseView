//
//  GoodsChooseSheet.m
//  SeaTradeProficient
//
//  Created by Bocweb on 2017/10/16.
//  Copyright © 2017年 boc.company. All rights reserved.
//
#define RGBAColor(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:a]
#define RGBColor(r, g, b) RGBAColor(r,g,b,1.0)
#define kUIScreenSize [UIScreen mainScreen].bounds.size
#define kUIScreenWidth kUIScreenSize.width
#define kUIScreenHeight kUIScreenSize.height
// 弱引用
#define kWeakSelf(type)  __weak typeof(type) weak##type = type
#import "ChooseSheet.h"

@interface ChooseSheet ()<UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong) UIView         *containerView;
@property (nonatomic, strong) UIPickerView   *pickerView;
@property (nonatomic, strong) ChooseModel    *chooseModel;
@end

@implementation ChooseSheet

- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:0.5]];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickEmpty:)];
        [self addGestureRecognizer:tap];
        
        [self makeUI];
    }
    return self;
}
- (void)setDataArray:(NSMutableArray *)dataArray{
    _dataArray = dataArray;
    self.chooseModel = [dataArray firstObject];
    [_pickerView reloadAllComponents];
}
- (void)makeUI {
    
    _containerView = [[UIView alloc] initWithFrame:CGRectMake(0, kUIScreenHeight-250, kUIScreenWidth, 250)];
    _containerView.backgroundColor = [UIColor whiteColor];
    _containerView.layer.cornerRadius = 3;
    _containerView.alpha = 0;
    _containerView.layer.masksToBounds = YES;
    _pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40,kUIScreenWidth, 210)];
    _pickerView.backgroundColor = [UIColor whiteColor];
    _pickerView.delegate = self;
    [_containerView addSubview:_pickerView];
    [self addSubview:_containerView];
    
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kUIScreenWidth, 40)];
    titleView.backgroundColor = RGBColor(249, 249, 249);
    [_containerView addSubview:titleView];
    
    
    UIButton *btnDone = [UIButton buttonWithType:UIButtonTypeCustom];
    btnDone.frame = CGRectMake(kUIScreenWidth - 50, 0, 40, 40);
    [btnDone setTitleColor:RGBColor(25, 132, 250) forState:UIControlStateNormal];
    [btnDone setTitle:@"确定" forState:UIControlStateNormal];
    [btnDone addTarget:self action:@selector(doneAction:) forControlEvents:UIControlEventTouchUpInside];
    btnDone.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [_containerView addSubview:btnDone];
    
    UIButton *btnCancel = [UIButton buttonWithType:UIButtonTypeCustom];
    btnCancel.frame = CGRectMake(10, 0, 40, 40);
    [btnCancel setTitleColor:RGBColor(25, 132, 250) forState:UIControlStateNormal];
    btnCancel.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
    [_containerView addSubview:btnCancel];
    
    kWeakSelf(self);
    [UIView animateWithDuration:0.25 animations:^{
        weakself.containerView.alpha = 1;
        weakself.containerView.top = kUIScreenHeight - 250;
    }];
    
    [self.pickerView reloadAllComponents];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    return self.dataArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    ChooseModel *model = self.dataArray[row];
     return model.title;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    ChooseModel *model = self.dataArray[row];
    NSString *text  = model.title;
    UILabel *label = [[UILabel alloc]init];
    [label setTextColor:[UIColor blackColor]];
    [label setTextAlignment:NSTextAlignmentCenter];
    [label setFont:[UIFont systemFontOfSize:20]];
    [label setText:text];
    return label;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    ChooseModel *model = self.dataArray[row];
    self.chooseModel   = model;
}

- (void)doneAction:(UIButton *)sender{
    [self confirmBtnClicked];
}

- (void)cancelAction:(UIButton *)sender{
    [self cancelBtnClicked];
}

- (void)clickEmpty:(UITapGestureRecognizer *)tap {
    [self cancelBtnClicked];
}

- (void)confirmBtnClicked
{
    if (self.nameBlock) {
        self.nameBlock(self.chooseModel);
    }
    [self cancelBtnClicked];
}

- (void)cancelBtnClicked
{
    kWeakSelf(self);
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0;
        weakself.containerView.frame = CGRectMake(0, kUIScreenHeight, kUIScreenWidth, 250);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
