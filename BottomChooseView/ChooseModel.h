//
//  GoodsModel.h
//  SeaTradeProficient
//
//  Created by Bocweb on 2017/10/16.
//  Copyright © 2017年 boc.company. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChooseModel : NSObject

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *depth;

@end
